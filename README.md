# fiction

Тексты для клуба писателей:
- [притча, услышанная по радио](parable-retelling.md)

- [общество одиночек](einsamkeitgesellschaft.md). Навеяно [рассказом](https://ocr.krossw.ru/html/mopassan/mopassan-odinochestvo-ls_1.htm) Мопассана
